const Userdb = require('../model/model');

//create and save new users
exports.create = (req, res) => {

    //validate the request
    if (!req.body) {
        res.status(400).send({ message: "Contente can note be empty" });
        return;
    }

    //new user data
    const user = new Userdb({
        name: req.body.name,
        email: req.body.email,
        gender: req.body.gender,
        status: req.body.status
    })

    //save user data in the DB
    user
        .save(user)
        .then(data => {
            // res.send(data)
            res.redirect('/')
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Error occured while creating user"
            })
        })
}

//retrieve and return all users
exports.find = (req, res) => {

    if (req.query.id) {
        const id = req.query.id;
        console.log('id', id)

        Userdb.findById(id)
            .then(data => {
                if (!data) {
                    res.status(400).send({ message: `This ${id} is not valid` })
                }
                else {
                    res.send(data)
                }
            })
            .catch(err => {
                res.status(500).send({ message: `Can not retrive the data` })
            })
    }
    else {
        Userdb.find()
            .then(user => {
                res.send(user)
            })
            .catch(err => {
                res.status(500).send({ message: err.message || "Can not retrieve the data" })
            })
    }

}

//update users
exports.update = (req, res) => {

    if (!req.body) {
        res.status(400).send({ message: "Contente can note be empty" });
        return;
    }

    const id = req.params.id;
    Userdb.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `Can not update user with ${id}` })
            }
            else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({ message: "Error occured while updating the user" })
        })

}

//Delete users
exports.delete = (req, res) => {
    const id = req.params.id

    Userdb.findByIdAndDelete(id)
        .then(data => {
            if (!data) {
                res.status(400).send({ message: `Can not delete, the ${id} is wrong` })
            }
            else {
                res.send({
                    message: "User delete successfully"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: `Couldn't delete with the ${id}`
            })
        })
}