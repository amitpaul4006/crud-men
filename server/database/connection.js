const mongoose = require('mongoose');

const connectDB = async () => {
    try {
        const con = await mongoose.connect(process.env.MONGO_URL);
        console.log(`MongoDB database connection established successfully`);

        // Access and log cluster info (assuming a replica set or sharded cluster)
        const host = con.connections[0].host;
        console.log(`Connected host: ${host}`);


    } catch (err) {
        console.error('Error connecting to MongoDB database:', err);
        process.exit(1); // Exit the process on failure
    }
};

module.exports = connectDB;
