const axios = require("axios");


exports.homeRoutes = (req, res) => {
    // Make get request for all users
    axios.get('http://localhost:3000/api/users')
        .then((response) => {
            const users = response.data; // Store the fetched data
            res.render('index', { users }); // Render with users data
        })
        .catch(err => {
            res.send(err); // Handle errors
        });
};

exports.add_user = (req, res) => {
    res.render('add_user');
};

exports.update_user = (req, res) => {
    axios.get('http://localhost:3000/api/users', { params: { id: req.query.id } })
        .then(function (userdata) {
            res.render("update_user", { user: userdata.data })
        })
        .catch(err => {
            res.send(err);
        })
}